# Desarrollo técnico Bellpi

### [URL de Aplicación](https://bellpi-test.vercel.app/)

Prueba de desarrollo para la empresa Bellpi, en donde se resuelve una aplicación con el API de Star Wars [SWAPI](https://swapi.dev/), funcionalidad de adición de láminas con información brindada por el api, recursos como Naves, personajes y Películas, cada una con estado regular o especial.

## Uso técnico

La aplicación se desarrollo bajo la librería [React JS](https://reactjs.org/), usando [Create React App CLI](https://create-react-app.dev/), bajo el lenguaje de programación **TypeScript**.

La interfaz visual, se utilizó el FrameWork css [Bulma](https://bulma.io/) y con el preprocesador de estilos [SCSS/SASS](https://sass-lang.com/documentation)

Para las peticiones HTTP del sitio se usó la librería [Axios](https://github.com/axios/axios) 

## Modo desarrollo.

- Agregar archivo .env para variables de entorno (Puede usar .env.example como referencia)
- Instalar módulos `npm install` o `yarn install`