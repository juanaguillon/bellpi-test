import React from "react";
import EnvelopeComponent from "../components/envelope-component";

interface GetSheetsProps {
  onSelectEnvelope: (a: number) => void;
  /** Trigger cuando se hayan seleccionado todas las láminas de un sobre.  */
  onCompleteEnvelope: () => void;
  selectedEnvelope: number;
  blockedEnvelopes: boolean;
}

export default class GetSheetsPage extends React.Component<GetSheetsProps> {
  onSelectEvelope(sel: number) {
    this.props.onSelectEnvelope && this.props.onSelectEnvelope(sel);
  }

  render() {
    return (
      <div id="getSheetsPage" style={{ marginTop: 24 }}>
        <section className="hero is-info">
          <div className="hero-body">
            <p className="title">Obtener láminas</p>
            <p className="subtitle">
              Aquí podrá obtener nuevas láminas para su álbum.
            </p>
          </div>
        </section>

        <div className="columns" style={{ marginTop: 24 }}>
          {new Array(4).fill("").map((_, i) => {
            return (
              <div className="column is-3" key={i}>
                <EnvelopeComponent
                  onSelectAll={this.props.onCompleteEnvelope}
                  blocked={
                    this.props.selectedEnvelope !== i &&
                    this.props.blockedEnvelopes
                  }
                  onClick={() => this.onSelectEvelope(i)}
                ></EnvelopeComponent>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

// const mapStateToProps = (state: SheetIface[]) => ({
//   sheetsStore: state,
// });

// export default connect(mapStateToProps)(GetSheetsPage);
