import React from "react";
import { connect } from "react-redux";
import { RenderSingleSheet, SheetIface } from "../components/sheet-component";
import { FilmIface } from "../services/filmService";
import { PeopleIface } from "../services/peopleService";
import { StarShipIface } from "../services/starShipService";

class MyAlbumPage extends React.Component<any> {
  renderEmptySheet = (id: number) => {
    return (
      <div className="card">
        <div className="card-content">
          <div className="content">
            <h4>ID: {id} </h4>
            <span>Lámina vacía</span>
          </div>
        </div>
      </div>
    );
  };

  render() {
    const sheets = this.props.sheets as SheetIface[];
    let films: SheetIface[] = [];
    let people: SheetIface[] = [];
    let starship: SheetIface[] = [];
    sheets.forEach((item) => {
      if (item.type === "fl") {
        films.push(item);
      } else if (item.type === "pe") {
        people.push(item);
      } else if (item.type === "ss") {
        starship.push(item);
      }
    });
    return (
      <div id="myalbumWrapper" style={{ marginTop: 24, marginBottom: 120 }}>
        <div className="columns is-multiline">
          <div className="column is-12">
            <h3 className="title has-text-center has-text-info">Películas</h3>
          </div>
          {films.map((item, key) => {
            if (!item.active) {
              return (
                <div key={key} className="column is-2">
                  {this.renderEmptySheet(item.id)}
                </div>
              );
            } else {
              return (
                <div className="column is-2">
                  <RenderSingleSheet
                    key={key}
                    readonly={true}
                    id={item.id}
                    iface={item.card as FilmIface}
                    special={item.special || false}
                    type={item.type || "fl"}
                  ></RenderSingleSheet>
                </div>
              );
            }
          })}

          <div className="column is-12 mt-5">
            <h3 className="title has-text-center has-text-info">Personajes</h3>
          </div>
          {people.map((item, key) => {
            if (!item.active) {
              return (
                <div key={key} className="column is-2">
                  {this.renderEmptySheet(item.id)}
                </div>
              );
            } else {
              return (
                <div className="column is-2">
                  <RenderSingleSheet
                    key={key}
                    readonly={true}
                    id={item.id}
                    iface={item.card as PeopleIface}
                    special={item.special || false}
                    type={item.type || "pe"}
                  ></RenderSingleSheet>
                </div>
              );
            }
          })}

          <div className="column is-12 mt-5">
            <h3 className="title has-text-center has-text-info">Naves</h3>
          </div>
          {starship.map((item, key) => {
            if (!item.active) {
              return (
                <div key={key} className="column is-2">
                  {this.renderEmptySheet(item.id)}
                </div>
              );
            } else {
              return (
                <div className="column is-2">
                  <RenderSingleSheet
                    key={key}
                    readonly={true}
                    id={item.id}
                    iface={item.card as StarShipIface}
                    special={item.special || false}
                    type={item.type || "ss"}
                  ></RenderSingleSheet>
                </div>
              );
            }
          })}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: any) => {
  return { sheets: state.sheets.storedSheets };
};

export default connect(mapStateToProps)(MyAlbumPage);
