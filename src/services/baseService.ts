import Axios, { AxiosRequestConfig, AxiosResponse } from "axios";

export default class BaseService {
  protected url = process.env.REACT_APP_BACKEND_URL;

  /**
   * Crear una petición GET
   * @param url URL de petición
   * @param options Headers necesarios
   * @version 1.0
   */
  protected async get<T = any>(
    url: string,
    options = {}
  ): Promise<AxiosResponse<T>> {
    return await Axios.get(url, options);
  }

  /**
   * Crear una petición POST
   * @param url URL de petición
   * @param params Data a ser enviada.
   * @param options Headers necesarios
   * @version 1.0
   */
  protected async post<T = any>(
    url: string,
    params: any,
    options: AxiosRequestConfig = {}
  ): Promise<AxiosResponse<T>> {
    return await Axios.post(url, params, options);
  }

  /**
   * Crear una petición PUT
   * @param url URL de petición
   * @param params Data a ser enviada.
   * @param options Headers necesarios
   * @version 1.0
   */
  protected async put<T = any>(
    url: string,
    params: any,
    options: AxiosRequestConfig = {}
  ): Promise<AxiosResponse<T>> {
    params["_method"] = "PUT";
    return await Axios.put(url, params, options);
  }

  /**
   * Crear una petición PATCH
   * @param url URL de petición
   * @param params Data a ser enviada.
   * @param options Headers necesarios
   * @version 1.0
   */
  protected async patch<T = any>(
    url: string,
    params: any,
    options: AxiosRequestConfig = {}
  ): Promise<AxiosResponse<T>> {
    return await Axios.patch(url, params, options);
  }

  /**
   * Crear una petición DELETE
   * @param url URL de petición
   * @param options Headers necesarios
   * @version 1.0
   */
  protected async delete<T = any>(
    url: string,
    options: AxiosRequestConfig = {}
  ): Promise<AxiosResponse<T>> {
    const params = {
      _method: "DELETE",
    };
    return await Axios.post(url, params, options);
  }
}
