import BaseService from "./baseService";

export interface StarShipIface {
  name: string;
  model: string;
  starship_class: string;
  manufacturer: string;
  cost_in_credits: string;
  length: string;
  crew: string;
  passengers: string;
  max_atmosphering_speed: string;
  hyperdrive_rating: string;
  MGLT: string;
  cargo_capacity: string;
  consumables: string;
  url: string;

  films: string[];
  pilots: string[];

  created: Date;
  edited: Date;
}

export default class StartShipService extends BaseService {
  /**
   * Obtener la información de nave por ID
   * @param id ID De nave a obtener
   * @returns Promise
   */
  public async getStarShipByID(
    id: number
  ): Promise<{ starship: StarShipIface | false }> {
    const url = `${this.url}/starships/${id}/`;
    try {
      const res = await this.get(url);
      return { starship: res.data };
    } catch (error) {
      return { starship: false };
    }
  }
}
