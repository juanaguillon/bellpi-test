import BaseService from "./baseService";

type peopeGender = "Male" | "Female" | "unknown" | "n/a";

export interface PeopleIface {
  birth_year: string;
  eye_color: string;
  gender: peopeGender;
  hair_color: string | "unknown" | "n/a";
  height: string;
  homeworld: string;
  mass: string;
  name: string;
  skin_color: string;

  films: string[];
  species: string[];
  starships: string[];
  vehicles: string[];
  url: string;

  created: Date;
  edited: Date;
}

export default class PeopleService extends BaseService {
  /**
   * Obtener la información de personaje por ID
   * @param id ID De personaje a obtener
   * @returns Promise
   */
  public async getPeopleByID(
    id: number
  ): Promise<{ people: PeopleIface | false }> {
    const url = `${this.url}/people/${id}/`;
    try {
      const res = await this.get(url);
      return { people: res.data };
    } catch (error) {
      return { people: false };
    }
  }
}
