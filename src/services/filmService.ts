import { getLastPartURL, uniqueArray } from "../utils/functions";
import BaseService from "./baseService";

export interface FilmIface {
  title: string;
  episode_id: number;
  opening_crawl: string;
  director: string;
  producer: string;
  url: string;

  species: string[];
  starships: string[];
  vehicles: string[];
  characters: string[];
  planets: string[];

  release_date: Date;
  created: Date;
  edited: Date;
}

/**
 * Clase para obtener peticiones reference a las películas
 */
export default class FilmService extends BaseService {
  /**
   * Obtener todas las urls de los personajes, películas y naves.
   * Se usa para obtener los IDs exactos de cada recurso.
   */
  public async getAllResourcesIDs() {
    const url = `${this.url}/films/`;
    const res = await this.get(url);

    const data = res.data.results;
    let urls: {
      films: number[];
      starships: number[];
      people: number[];
    } = {
      films: [],
      starships: [],
      people: [],
    };

    data.forEach((element: any) => {
      element.starships.map((st: string) => {
        return urls["starships"].push(parseInt(getLastPartURL(st)));
      });

      element.characters.map((st: string) => {
        return urls["people"].push(parseInt(getLastPartURL(st)));
      });

      urls["films"].push(parseInt(getLastPartURL(element.url)));
    });

    urls["starships"] = uniqueArray(urls["starships"]);
    urls["people"] = uniqueArray(urls["people"]);

    return urls;
  }

  /**
   * Obtener la información de película por ID
   * @param id ID De película a obtener
   * @returns Promise
   */
  public async getFilmByID(id: number): Promise<{ film: FilmIface | false }> {
    const url = `${this.url}/films/${id}/`;
    try {
      const res = await this.get(url);
      return { film: res.data };
    } catch (error) {
      return { film: false };
    }
  }
}
