import React from "react";
import { connect } from "react-redux";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import HeaderComponent from "./components/layout/header-component";
import { SheetIface } from "./components/sheet-component";

import GetSheetsPage from "./pages/get-sheets";
import MyAlbumPage from "./pages/my-album";

import FilmService from "./services/filmService";

import { addMultipleSheet } from "./store/sheet";

interface AppProps {
  addMultipleSheet: (arg: SheetIface[]) => void;
}

class App extends React.Component<AppProps> {
  countdownInterval: null | NodeJS.Timeout = null;

  readonly state = {
    /** Key único de Pagina de Láminas, funcional para resetear el componente de págna de láminas. */
    sheetPageKey: 1,

    countDown: 60,
    availableEnvelopes: 4,
    selectedEnvelope: 0,
    blockedEnvelopes: false,

    showEvelopesWrap: true,
  };

  /**
   * Procesar las láminas sin contenido.
   * Se guardan todos los IDs de los recursos en el store para posteriormente usarlos en los sobres de láminas.
   */
  public processResource(resouce: number[], type: "ss" | "pe" | "fl") {
    let seetsToAdd = [];
    for (let i = 0; i < resouce.length; i++) {
      const element: SheetIface = {
        id: resouce[i],
        active: false,
        type: type,
      };
      seetsToAdd.push(element);
    }

    this.props.addMultipleSheet(seetsToAdd);
  }

  async componentDidMount() {
    if (window.location.pathname !== "/") {
      this.setState({
        showEvelopesWrap: false,
      });
    }

    const filmsService: FilmService = new FilmService();
    const res = await filmsService.getAllResourcesIDs();

    this.processResource(res.films, "fl");
    this.processResource(res.people, "pe");
    this.processResource(res.starships, "ss");
  }

  initCountDown = (selectedEnvelope: number) => {
    this.setState({
      blockedEnvelopes: true,
      selectedEnvelope,
    });
    this.countdownInterval = setInterval(() => {
      let nCount = this.state.countDown;
      this.setState({
        countDown: --nCount,
      });

      if (this.state.countDown < 1) {
        this.setState({
          blockedEnvelopes: false,
          countDown: 60,
        });
        clearInterval(this.countdownInterval as NodeJS.Timeout);
      }
    }, 1000);
  };

  /**
   * Cuando el header envíe notificación que cambió de path
   * @param newRoute Nueva ruta
   */
  onRouteChange = (newRoute: string) => {
    this.setState({
      showEvelopesWrap: newRoute === "/",
    });
  };

  /**
   * Cuando se de click en el botón "Restaurar sobres"
   * Solo se mostrará este botón cuando this.state.availableEnvelopes === 0
   */
  onLoadMoreEvenlopes = () => {
    clearInterval(this.countdownInterval as NodeJS.Timeout);

    this.setState({
      sheetPageKey: this.state.sheetPageKey + 1,
      availableEnvelopes: 4,
      selectedEnvelope: 0,
      blockedEnvelopes: false,
      countDown: 60,
    });
  };

  /**
   * FUncionalidad cuando todos las láminas de un sobre sean seleccionadas.
   */
  onCompleteEnvelpe = () => {
    let avEnvelopes = this.state.availableEnvelopes;
    this.setState({
      availableEnvelopes: avEnvelopes - 1,
    });
  };

  render() {
    let sheetWrapDisplay = "block";
    if (!this.state.showEvelopesWrap) {
      sheetWrapDisplay = "none";
    }
    return (
      <div className="App">
        <Router>
          <HeaderComponent onChangeRoute={this.onRouteChange}></HeaderComponent>
          <div className="container">
            <div
              className="sheets-page-wrap"
              style={{ display: sheetWrapDisplay }}
            >
              <GetSheetsPage
                key={this.state.sheetPageKey}
                selectedEnvelope={this.state.selectedEnvelope}
                blockedEnvelopes={this.state.blockedEnvelopes}
                onCompleteEnvelope={this.onCompleteEnvelpe}
                onSelectEnvelope={this.initCountDown}
              />

              {this.state.availableEnvelopes === 0 && (
                <div className="has-text-centered mt-5">
                  <button
                    className="button is-primary"
                    onClick={this.onLoadMoreEvenlopes}
                  >
                    Restaurar Sobres
                  </button>
                </div>
              )}
            </div>

            <Switch>
              <Route exact path="/my-album">
                <MyAlbumPage />
              </Route>
            </Switch>
            <div className="contador">
              <span>{this.state.countDown}</span>
            </div>
          </div>
        </Router>
      </div>
    );
  }
}

export default connect(null, { addMultipleSheet })(App);
