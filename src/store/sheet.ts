import { combineReducers } from "redux";
import { SheetIface } from "../components/sheet-component";

const ADD_MULTIPLE_SHEET = "ADD_MULTIPLE_SHEET";
const TOGGLE_SHEET_BY_ID_TYPE = "TOGGLE_SHEET_BY_ID_TYPE";

export function addMultipleSheet(newSheets: SheetIface[]) {
  return {
    type: ADD_MULTIPLE_SHEET,
    sheets: newSheets,
  };
}

export function updateSheet(sheet: SheetIface) {
  return {
    type: TOGGLE_SHEET_BY_ID_TYPE,
    sheet,
  };
}

interface StoreIface {
  storedSheets: SheetIface[];
  special: {
    films: number;
    people: number;
    starships: number;
  };
}

const defaultSheets: StoreIface = {
  storedSheets: [],
  special: {
    films: 0,
    people: 0,
    starships: 0,
  },
};

function sheets(state: StoreIface = defaultSheets, action: any) {
  switch (action.type) {
    case ADD_MULTIPLE_SHEET:
      const newState = {
        ...state,
        storedSheets: state.storedSheets.concat(action.sheets),
      };

      return newState;

    case TOGGLE_SHEET_BY_ID_TYPE:
      let newSheet = action.sheet as SheetIface;
      const keySheet = state.storedSheets.findIndex(
        (item) => item.id === newSheet.id && item.type === newSheet.type
      );

      if (newSheet.type === "fl") {
        state.special.films++;
      } else if (newSheet.type === "pe") {
        state.special.people++;
      } else if (newSheet.type === "ss") {
        state.special.starships++;
      }

      state.storedSheets[keySheet] = action.sheet;

      return state;

    default:
      return state;
  }
}

export default combineReducers({
  sheets,
});
