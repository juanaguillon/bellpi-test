import React from "react";

interface LoaderProps {
  id?: string;
  show?: boolean;
}
const Loader = (props: LoaderProps) => {
  let cssName = "app-loader-wrapper " + (props.id || "");
  if (props.show) {
    cssName += " show";
  }

  return (
    <div className={cssName}>
      <div className="app-loader">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </div>
    </div>
  );
};

export { Loader };
