import React from "react";
import { connect } from "react-redux";
import FilmService, { FilmIface } from "../services/filmService";
import PeopleService, { PeopleIface } from "../services/peopleService";
import StartShipService, { StarShipIface } from "../services/starShipService";
import { updateSheet } from "../store/sheet";

import { generateRandMinMax, shuffleArray } from "../utils/functions";
import { Loader } from "./general-components";
import { RenderSingleSheet, SheetIface, SheetType } from "./sheet-component";

interface EnvolveComponentProps {
  blocked?: boolean;
  onClick: () => void;
  /** Cuando se hayan seleccionado todas las láminas de este sobre. */
  onSelectAll: () => void;

  sheets: SheetIface[];
  special: {
    films: number;
    people: number;
    starships: number;
  };
  updateSheet: (a: SheetIface) => any;
}
interface EnvolveComponentState {
  modalActive: boolean;
  cards: {
    iface: FilmIface | StarShipIface | PeopleIface;
    type: string;
    id: number;
  }[];
  /** Si aún no se han cargado recursos de este sobre. */
  initialized: boolean;
}

class EnvelopeComponent extends React.Component<
  EnvolveComponentProps,
  EnvolveComponentState
> {
  config: SheetType[] = [];

  filmService: FilmService = new FilmService();
  peopleService: PeopleService = new PeopleService();
  starShipService: StartShipService = new StartShipService();

  readonly state: EnvolveComponentState = {
    modalActive: false,
    initialized: false,
    cards: [],
  };

  constructor(props: any) {
    super(props);
    this.setEvelopeConfig();
  }

  /**
   * Inicializar la configuración del actual sobre.
   * Cada sobre contendrá una configuración aleatoria, asignada en este función.
   * Los valores son: ss -> Starship, pe -> People y fl -> Film
   */
  setEvelopeConfig() {
    const conf1: SheetType[] = ["ss", "pe", "pe", "pe", "fl"];
    const conf2: SheetType[] = ["pe", "pe", "pe", "ss", "ss"];

    // Revolver valores de configuración
    shuffleArray(conf1);
    shuffleArray(conf2);

    const confs = [conf1, conf2];

    let max = confs.length - 1;
    this.config = confs[generateRandMinMax(0, max)];
  }

  /**
   * Cuando se de click en el sobre.
   * Esto comenzará a buscar los recursos (En caso de no haberlos buscado anteriormente) y abrirá el modal.
   */
  onTapEnvolpe = async () => {
    if (this.props.blocked) return false;

    this.setState({
      modalActive: true,
    });

    // No se ejecutarán el trigger (Para resetear el contador) ni cargar nuevos recursos de caso de que hayan cards.
    if (this.state.initialized) return false;

    this.props.onClick();

    const ss = this.props.sheets.filter((item) => item.type === "ss");
    const pe = this.props.sheets.filter((item) => item.type === "pe");
    const fl = this.props.sheets.filter((item) => item.type === "fl");

    const resources = [];
    for (let index = 0; index < this.config.length; index++) {
      const type = this.config[index];
      let item;
      switch (type) {
        case "ss":
          item = ss[Math.floor(Math.random() * ss.length)];
          let resourceSs = await this.starShipService.getStarShipByID(item.id);
          resourceSs.starship &&
            resources.push({
              id: item.id,
              iface: resourceSs.starship,
              type: "ss",
            });
          break;
        case "pe":
          item = pe[Math.floor(Math.random() * pe.length)];
          let resourcePe = await this.peopleService.getPeopleByID(item.id);
          resourcePe.people &&
            resources.push({
              id: item.id,

              iface: resourcePe.people,
              type: "pe",
            });

          break;
        case "fl":
          item = fl[Math.floor(Math.random() * fl.length)];
          let resourceFl = await this.filmService.getFilmByID(item.id);
          resourceFl.film &&
            resources.push({
              id: item.id,
              iface: resourceFl.film,
              type: "fl",
            });
          break;
      }
    }
    this.setState({ cards: resources, initialized: true });
  };

  /**
   * Contenido que se mostrará en el modal cuando no haya ninguna lámina para seleccionar.
   */
  renderEmptyEnvelope() {
    return (
      <div className="card">
        <div className="card-content">
          <div className="content">
            <h4 className="has-text-danger-dark">Sobre vacío</h4>
            <p>Ya se han seleccionado todas láminas de este sobre.</p>
          </div>
        </div>
      </div>
    );
  }

  renderSheetsCounter() {
    return this.state.cards.length > 0 ? (
      <span className="has-text-success-dark">
        {this.state.cards.length} láminas disponibles
      </span>
    ) : (
      <span className="has-text-danger-dark">Sin láminas para seleccionar</span>
    );
  }

  /**
   * Verificar si la lámina actual ya está en "Mi album"
   */
  getIfIsStored = (ID: number, type: SheetType) => {
    let sheets = this.props.sheets;
    let isStored = sheets.find((item) => {
      return item.id === ID && item.type === type && item.active;
    });

    return typeof isStored !== "undefined";
  };

  /**
   * Verificar si la lámina que será agregada será especial o no.
   * @param type Tipo de lámina a verificar
   * @returns
   */
  getIfIsSpecial = (type: SheetType): boolean => {
    let special = this.props.special;
    if (type === "fl") {
      return special.films <= 6;
    } else if (type === "pe") {
      return special.people <= 20;
    } else if (type === "ss") {
      return special.starships <= 10;
    } else {
      return false;
    }
  };

  render() {
    let modalClass: string = "modal";
    if (this.state.modalActive) {
      modalClass += " is-active";
    }

    let cardClass = "card envelope-card";
    if (this.props.blocked) {
      cardClass += " blocked";
    }

    return (
      <div className="envelope-cart-wrapper">
        <div className={cardClass} onClick={this.onTapEnvolpe}>
          <div className="card-content" style={{ textAlign: "center" }}>
            <div className="media">
              <div className="media-content">
                <p className="title is-4">+</p>
                {this.state.initialized && this.renderSheetsCounter()}
                <p className="subtitle is-6">Ver Sobre</p>
              </div>
            </div>
          </div>
        </div>

        <div className={modalClass}>
          <Loader
            show={this.state.cards.length === 0 && !this.state.initialized}
          ></Loader>
          <div
            className="modal-background"
            onClick={() => this.setState({ modalActive: false })}
          ></div>
          <div className="modal-content">
            {this.state.cards.length === 0 &&
              this.state.initialized &&
              this.renderEmptyEnvelope()}

            <div className="envelope-sheets">
              {this.state.cards.map((item, i) => {
                const isSpecial = this.getIfIsSpecial(item.type as SheetType);
                const isStored = this.getIfIsStored(
                  item.id,
                  item.type as SheetType
                );
                return (
                  <RenderSingleSheet
                    key={i}
                    canAdded={!isStored}
                    id={item.id}
                    special={isSpecial}
                    iface={item.iface}
                    type={item.type as SheetType}
                    onClick={() => {
                      let newCards = [...this.state.cards];
                      newCards.splice(i, 1);
                      if (newCards.length === 0) {
                        this.props.onSelectAll && this.props.onSelectAll();
                      }

                      this.setState({
                        cards: newCards,
                      });
                    }}
                    onAccept={() => {
                      this.props.updateSheet({
                        id: item.id,
                        active: true,
                        card: item.iface,
                        type: item.type as SheetType,
                        special: isSpecial,
                      });
                    }}
                  ></RenderSingleSheet>
                );
              })}
            </div>
          </div>
          <button
            className="modal-close is-large"
            aria-label="close"
            onClick={() => this.setState({ modalActive: false })}
          ></button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    sheets: state.sheets.storedSheets,
    special: state.sheets.special,
  };
};

const mapDispatchToProps = () => {
  return {
    updateSheet,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps()
)(EnvelopeComponent);
