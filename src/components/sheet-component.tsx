import React from "react";
import { FilmIface } from "../services/filmService";
import { PeopleIface } from "../services/peopleService";
import { StarShipIface } from "../services/starShipService";

export type SheetType = "ss" | "pe" | "fl";

export interface SheetIface {
  id: number;
  active: boolean;
  type?: SheetType;
  special?: boolean;
  card?: FilmIface | PeopleIface | StarShipIface;
}

interface RenderSingleSheetProps {
  id: number;

  iface: FilmIface | StarShipIface | PeopleIface;
  type: SheetType;
  special: boolean;
  canAdded?: boolean;
  readonly?: boolean;
  /** Cuando de click en botón de agregar o botón de descartar */
  onClick?: () => void;
  /** Cuando de click en botón de agregar. */
  onAccept?: () => void;
}
export const RenderSingleSheet = (props: RenderSingleSheetProps) => {
  let cardVals: {
    title: string;
    subtitle: string;
    date: Date;
    args: any[];
  };
  switch (props.type) {
    case "fl":
      let flface = props.iface as FilmIface;
      cardVals = {
        title: flface.title,
        subtitle: flface.producer,
        date: flface.created,
        args: [
          {
            title: "Director",
            value: flface.director,
          },
          {
            title: "Lanzamiento",
            value: flface.release_date,
          },
        ],
      };
      break;
    case "pe":
      let peface = props.iface as PeopleIface;
      cardVals = {
        title: peface.name,
        subtitle: peface.gender,
        date: peface.created,
        args: [
          {
            title: "Altura",
            value: peface.height,
          },
          {
            title: "Masa",
            value: peface.mass,
          },
          {
            title: "Color de Piel",
            value: peface.skin_color,
          },
        ],
      };
      break;
    case "ss":
      let ssface = props.iface as StarShipIface;
      cardVals = {
        title: ssface.name,
        subtitle: ssface.length,
        date: ssface.created,
        args: [
          {
            title: "Consumibles",
            value: ssface.consumables,
          },
          {
            title: "Clase",
            value: ssface.starship_class,
          },
        ],
      };
      break;
  }

  return (
    <SheetComponent
      onTapButton={() => props.onClick && props.onClick()}
      onTapAccept={() => props.onAccept && props.onAccept()}
      type={props.type}
      id={props.id}
      canAdded={props.canAdded}
      readonly={props.readonly}
      special={props.special}
      {...cardVals}
    ></SheetComponent>
  );
};

interface SheetComponentProps {
  id: number;
  type: SheetType;
  title: string;
  subtitle: string;
  date: Date;
  args: { title: string; value: string }[];
  special: boolean;
  readonly?: boolean;
  canAdded?: boolean;
  onTapButton?: Function;
  onTapAccept?: Function;
}

class SheetComponent extends React.Component<SheetComponentProps> {
  render() {
    let types = {
      ss: "Nave",
      pe: "Personaje",
      fl: "Película",
    };

    return (
      <div id={this.props.type + this.props.id} className="sheet-wrapper">
        <div className="card">
          <div className="card-content">
            <div className="sheet-tag-wrap">
              {this.props.special ? (
                <span style={{ marginRight: 4 }} className="tag is-warning">
                  Especial
                </span>
              ) : (
                <span style={{ marginRight: 4 }} className="tag is-dark">
                  Regular
                </span>
              )}
              <span className="tag is-info">{types[this.props.type]}</span>
            </div>

            <div className="media">
              <div className="media-content">
                <p className="title is-4">{this.props.title}</p>
                <p className="subtitle is-6">{this.props.subtitle}</p>
              </div>
            </div>
            <div className="content">
              {this.props.args.map((item, i) => {
                return (
                  <span key={i}>
                    <strong>{item.title}</strong>: {item.value}
                    <br />
                  </span>
                );
              })}
              <time>{this.props.date}</time>
              {!this.props.readonly && (
                <div className="buttons" style={{ marginTop: 12 }}>
                  {this.props.canAdded ? (
                    <button
                      className="button is-success is-small"
                      onClick={() => {
                        this.props.onTapButton && this.props.onTapButton();
                        this.props.onTapAccept && this.props.onTapAccept();
                      }}
                    >
                      + Agregar
                    </button>
                  ) : (
                    <button
                      className="button is-danger is-small"
                      onClick={() =>
                        this.props.onTapButton && this.props.onTapButton()
                      }
                    >
                      Descartar
                    </button>
                  )}
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
