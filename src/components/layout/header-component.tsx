import React from "react";
import { Link } from "react-router-dom";

export default class HeaderComponent extends React.Component<any> {
  render() {
    return (
      <nav
        className="navbar is-dark"
        role="navigation"
        aria-label="main navigation"
      >
        <div className="navbar-brand">
          <button
            className="navbar-burger"
            type="button"
            aria-label="menu"
            aria-expanded="false"
            data-target="navbarBasicExample"
          >
            <span aria-hidden="true" />
            <span aria-hidden="true" />
            <span aria-hidden="true" />
          </button>
        </div>
        <div id="navbarBasicExample" className="navbar-menu">
          <div className="navbar-start">
            <Link
              to="/"
              className="navbar-item"
              onClick={() => this.props.onChangeRoute("/")}
            >
              Obtener láminas
            </Link>
            <Link
              to="/my-album"
              className="navbar-item"
              onClick={() => this.props.onChangeRoute("/my-album")}
            >
              Mi Álbum
            </Link>
          </div>
        </div>
      </nav>
    );
  }
}
