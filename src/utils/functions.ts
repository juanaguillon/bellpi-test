/**
 * Obtener un número aleatorio entre un rango mínimo y máximo
 * @param {number} min Número mínimo
 * @param {number} max Número máximo
 * @returns number
 */
const generateRandMinMax = function (min: number, max: number) {
  return Math.floor(Math.random() * (max - 0 + 1) + 0);
};

/**
 * Obtener la última parte de una URL. Será útil para identificar los ids de los recursos.
 * @param url URL a ser procesada
 * @returns string
 */
const getLastPartURL = function (url: string) {
  let urlParts = url.split("/").filter((i) => i !== "");
  return urlParts[urlParts.length - 1];
};

/**
 * Limpiar y eliminar elementos de un array repetidos.
 * @param prevArr Array previo
 * @returns any[]
 */
const uniqueArray = function (prevArr: any[]) {
  return prevArr.filter(function (item, pos) {
    return prevArr.indexOf(item) === pos;
  });
};

/**
 * Mezcalar y randomizar elementos de un array
 * @param arr Array a ser alterado
 * @returns any[]
 */
const shuffleArray = function (arr: any[]) {
  var currentIndex = arr.length,
    temporaryValue,
    randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {
    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = arr[currentIndex];
    arr[currentIndex] = arr[randomIndex];
    arr[randomIndex] = temporaryValue;
  }

  return arr;
};

export { generateRandMinMax, getLastPartURL, uniqueArray, shuffleArray };
